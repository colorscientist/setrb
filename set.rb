module SetRb
    # The main class which is intended for user consumption, represents
    # a set and the operations which can be performed on it
    class Set
        def initialize inputs
            @members = Array.new
            inputs.each do |input|
                if not @members.include? input
                    @members.push input
                end
            end
        end

        def members
            @members
        end

        def include? item
            if @members.include? item
                return true
            else
                return false
            end
        end

        def length
            return @members.length
        end

        def isnull?
            if self.length != 0
                false
            else
                true
            end
        end

        def add input
            if not self.include? input
                @members.push input
            end
        end

        # Returns all members which are in both sets
        def intersection set
            intersection_members = Set.new []
            set.members.each do |member|
                if self.include? member
                    intersection_members.add member
                end
            end
            return intersection_members
        end

        # Alias & to intersection
        def &(set)
            return self.intersection(set)
        end

        # Returns all members of any set provided
        def union set
            union_members = Set.new []
            set.members.each do |member|
                union_members.add member
            end
            @members.each do |member|
                union_members.add member
            end
            return union_members
        end

        # Alias | to union
        def |(set)
            return self.union(set)
        end
       
        # Do the two sets share no members? 
        def disjoint? set
            intersection = self.intersection set

            if intersection.isnull?
                return true
            else
                return false
            end
        end

        # Elements not in this set
        def complement set
            complement_members = Set.new []

            @members.each do |member|
                if not set.include? member
                    complement_members.add member
                end
            end

            return complement_members
        end

        def symmetric_diff set
            return self.union(set).complement(self.intersection(set))
        end

        def cart_prod set
            prod_set = Set.new []
            @members.each do |member|
                set.members.each do |other_member|
                    prod_set.add [[member, other_member]]
                end
            end
            return prod_set
        end

        def to_s
            "{#{@members.join(", ")}}"
        end
    end

    # A small demonstration of the library functions, with expected results
    set = Set.new ['a', 'b', 'a']
    set.add 'c'
    set.add 'a'
    puts set # ==> {a, b, c}
    puts set.isnull? # ==> false
    puts set.union Set.new ['b', 'd']  # ==> {a, b, c, d}
    puts (set | Set.new(['b', 'd'])).members == set.union(Set.new ['b', 'd']).members # ==> true
    puts set.intersection Set.new ['b', 'd'] # ==> {b}
    puts (set & Set.new(['b', 'd'])).members == set.intersection(Set.new ['b', 'd']).members # ==> true
    puts set.disjoint? Set.new ['a'] # ==> false
    puts set.disjoint? Set.new ['f'] # ==> true
    puts set.complement Set.new ['a']  # ==> {b, c}
    puts set.symmetric_diff Set.new ['a', 'd']  # ==> {b, c, d}
    puts set.cart_prod Set.new ['a', 'c'] # ==> {('a', 'a'), ('a', 'c') ... }
end
